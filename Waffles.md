# Waffles
## Ingredients

### Dry Mix
+ ___2cups___ Flour
+ ___4tbsp___ Sugar
+ ___4tsp___ Baking Powder
+ ___1/2tsp___ Salt
+ ___1tsp___ Ground Cinnamon `optional`
+ ___1/2cup___ Belgian Pearl Sugar `optional`

### Wet Mix
+ ___2___ Eggs
+ ___1 1/2cups___ Milk
+ ___6tbsp___ Butter `melted`
+ ___1tsp___ Vanilla Extract

## Preparation
1. In a large bowl, combine `dry mix ingredients,` except for `pearl sugar.` Create a well in the center.
2. In a second bowl, whisk `eggs` into `milk` until well combined.
3. Add `melted butter and vanilla` into `wet mix.`
4. Pour `wet mix` into `dry mix,` whisk until blended. Batter should be slightly lumpy. Fold in `pearl sugar` if desired.
5. `1cup` at a time, cook mixture in waffle iron.