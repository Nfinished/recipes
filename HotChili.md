# Hot Chili
## Ingredients

+ ___1lb___ Ground Beef
+ ___1lb___ Ground Pork
+ ___1___ Red Onion `diced`
+ ___3___ Garlic Cloves `minced`
+ ___1/4lb___ Jalapeño Peppers `minced`
+ ___4oz (1 can)___ Mild Chopped Chili Peppers
+ ___29oz (2 cans)___ Diced Roasted Tomatoes `undrained`
+ ___1tbsp___ Chili Powder
+ ___1tbsp___ Chipotle Chili Powder
+ ___1/2tsp___ Ground Cumin
+ ___1tsp___ Hot Sauce
+ ___2tbsp___ Balsamic Vinegar
+ Shredded Cheese `optional`
+ Salt and Pepper

## Preparation

1. Brown the `ground beef and pork` with the `red onion` in a pot over ___medium-high___ heat. Liberally salt and pepper.
2. Stir in `garlic, chili peppers, and jalapeño`, cooking for another minute.
3. Add `tomatoes, chili powders, cumin, hot sauce, and vinegar.' Stir to mix, then bring to a simmer. Reduce heat to ___low___ and simmer for another ___15-20___ minutes.
4. Serve and top with cheese.
