# Egg Drop Soup
## Ingredients

+ ___2cups___ Chicken Stock
+ ___3___ Scallions `white and green separated`
+ ___1 1/2tbsp___ Cornstarch
+ ___2___ Eggs `beaten`
+ Spices


## Preparation
1. Bring `chicken stock and white scallion` to a boil in a medium pot, reserving ___1/2tbsp___ of `chicken stock.`
2. Create a slurry of `reserved chicken stock and cornstarch.`
3. When pot boils, slowly add `cornstarch slurry` to pot, ensuring it doesn't clump.
4. Add `eggs` while stirring gently, boil for another `1 minute.`
5. Garnish with `green scallion and spices` to taste.