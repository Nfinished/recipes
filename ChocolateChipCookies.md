# Chocolate Chip Cookies
## Ingredients

+ ___2sticks___ Salted Butter `melted`
+ ___2cups___ Sugar
+ ___1tbsp___ Molasses
+ ___2___ Eggs
+ ___1tsp___ Vanilla Extract
+ ___2tsp___ Salt
+ ___1tsp___ Baking Soda
+ ___2 1/2cups___ Bread Flour
+ ___12oz___ Chocolate Chips

## Preparation

1. Mix `butter, sugar, molasses, eggs, salt, and vanilla` until smooth.
2. Add in `baking soda and flour,` adding more flour until dough appears wet but not sticky.
3. Fold in `chocolate chips.`
4. Refrigerate for ___1 hour___.
5. Form `chilled dough` into golf ball sized peices, then flatten into pucks. Arrange on parchment-lined baking sheets.
6. Broil on a high rack until tops are golden.
7. Remove and set oven to bake at ___375°F___.
8. After oven has cooled to baking temperature, bake for 10-12 minutes or until done.