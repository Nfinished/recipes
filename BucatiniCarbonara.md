# Bucatini Carbonara
## Ingredients

+ ___1/2lb___ Thick Cut Bacon or Bacon Ends `chunked`
+ ___4z___ Grated Parmesan
+ ___3___ Large Eggs
+ ___1___ Egg Yolk
+ ___3___ Garlic Cloves `minced`
+ ___16oz___ Bucatini
+ Salt and Pepper

## Preparation

1. Beat `eggs and yolk` together in a bowl, mix in `parmesan` and set aside.
2. Cook `bacon` over ___medium___ medium heat until cooked through and fat has rendered out.
3. Strain out grease, keeping about ___2tbsp___ in the pan.
4. Reduce heat to ___low___, add `garlic` and saute for ___60___ seconds. Turn off the heat.
5. Add `bucatini` to a boiling pot of salted water and cook to desired doneness. Drain, reserving ___1/3cup___ pasta water.
6. Add `bucatini` to the pan along with reserved `pasta water.` Pour in `egg mixture` and toss vigorously.
7. If pasta seems watery, add more `cheese` until desired consistency is met. Serve.
