# Cheddar Quickbread
## Ingredients

### Dry Mix
+ ___2cups___ All-Purpose Flour
+ ___1/4cup___ White sugar
+ ___1 1/4cup___ Cheddar Cheese `shredded`
+ ___1 1/2tsp___ Baking Powder
+ ___1/2tsp___ Baking Soda
+ ___1tsp___ Salt

### Wet Mix
+ ___1cup___ Buttermilk
+ ___1___ Large Egg
+ ___1/4cup___ Salted Butter `melted`

## Preparation

1. Heat oven to ___350 degrees.___ Grease a 9x5 loaf pan.
2. Combine dry and wet mixes separately, then pour wet into dry.
3. Gently stir and fold until dry mix is completely incorporated and a shaggy, wet batter has formed. Be careful not to overmix.
4. Scrape batter into the pan and pat down into corners, making sure it's evenly distributed.
5. Bake for ___50 minutes.___ Let cool in pan for ___15 minutes___ before serving.