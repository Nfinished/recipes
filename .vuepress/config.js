module.exports = {
    title: 'Nfinished Recipes',
    description: "Cholesterall my favorite recipes in one place.",
    head: [
        ['link', { rel: 'icon', href: `http://unfinished.business/favicon.png` }]
    ],
    dest: 'public',
    ga: 'UA-68038797-5',
    themeConfig: {
        repo: 'https://gitlab.com/Nfinished/recipes',
        nav: [
            { text: 'Back To Business', link: 'http://unfinished.business' }
        ],
        sidebarDepth: 0,
        sidebar: [
            ['/BBQFriedRice', 'BBQ Fried Rice'],
            ['/BeefStew', 'Beef Stew'],
            ['/BeerCheeseDip', 'Beer Cheese Dip'],
            ['/BraisedKnockwurst', 'Braised Knockwurst'],
            ['/BucatiniCarbonara', 'Bucatini Carbonara'],
            ['/BuffaloChickenDip', 'Buffalo Chicken Dip'],
            ['/BuffaloMacAndCheese', "Buffalo Mac 'n Cheese"],
            ['/CarmelizedOnions', 'Carmelized Onions'],
            ['/CastIronPizza', 'Cast Iron Pizza'],
            ['/CheddarQuickbread', 'Cheddar Quickbread'],
            ['/ChickenKungPao', "Chicken Kung Pao"],
            ['/ChickenNuggets', 'Chicken Nuggets'],
            ['/ChocolateChipCookies', 'Chocolate Chip Cookies'],
            ['/CornedBeefHash', 'Corned Beef Hash'],
            ['/EggDropSoup', 'Egg Drop Soup'],
            ['/GarlicButterSteak', 'Garlic Butter Steak'],
            ['/GeneralTsosChicken', "General Tso's Chicken"],
            ['/HotChili', 'Hot Chili'],
            ['/MexicanRice', 'Mexican Rice'],
            ['/OrangeChicken', 'Orange Chicken'],
            ['/RoastPotatoes', 'Roast Potatoes'],
            ['/ShrimpSpinachPasta', 'Spicy Shrimp & Spinach Pasta'],
            ['/Waffles', 'Waffles']
        ]
    },
    markdown: {
        anchor: {
            permalink: false
        }
    }
}
