# Mexican Rice
## Ingredients

+ ___1tbsp___ Tomato Paste
+ ___1/2tsp___ Onion Powder
+ ___1/2tsp___ Garlic Powder
+ ___1cup___ Long Grain White Rice
+ ___3tbsp___ Corn Oil
+ ___1 1/2cup___ Chicken Stock
+ ___1tsp___ Salt

## Preparation

1. In a small bowl, mix together `tomato paste, onion and garlic powders, and 1/4cup water` until homogenous. Set aside.
2. Heat `corn oil` in a medium saucepan over ___medium-high___ heat. Once hot, add `rice,` stirring until toasted.
3. Add in `tomato mixture` and cook down, stirring constantly ___45-60sec___.
4. Stir in `Chicken Stock and salt,` bring to a boil and cover.
5. Reduce heat to ___low___, allow to simmer for ___20min___.
6. Remove lid briefly to allow rice to breathe, take off heat and let to rest for ___10min___.