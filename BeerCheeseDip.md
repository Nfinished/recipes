# Beer Cheese Dip
## Ingredients

+ ___2lb___ Sharp Cheddar `1/2in cubed`
+ ___1can___ Beer
+ ___1/4cup___ Red Pepper `chopped`
+ ___1/4cup___ Red Onion `chopped`
+ ___1___ Jalapeño `diced`
+ ___1tbsp___ Cornstarch
+ Kosher Salt, Pepper, Cayenne Pepper

## Preparation

1. Toss `cheddar cubes` with `cornstarch.`
2. Bring `beer` to a boil in a cast iron pan over ___medium-high___ heat. Slowly add `cheese` and mix until smooth.
3. Mix in `red pepper,` add `salt and cayenne pepper` to taste.
4. Broil for ___3-5 minutes___ or until top is golden brown, garnish with `red onion and jalapeno.`
