# Carmelized Onions
## Ingredients

+ ___6 tbsp___ Unsalted Butter
+ ___3 lbs___ Yellow or Mixed Onions (Red, Yellow, Shallot)
+ Salt, Pepper, Spices

## Preparation

1. In a large stainless steel saucepan, melt `butter` over ___medium-high___ heat until foaming.
2. Add onions and cook for about ___8min___ or until soft, stirring occasionally.
3. Lower heat to ___medium-low___ and cook ___1-2___ hours, stirring frequently until `onions` are sweet and golden brown. If onion starts to burn, add ___1tbsp___ of `water` and continue stirring.
4. Season with salt and pepper.