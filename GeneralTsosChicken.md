# General Tso's Chicken
## Ingredients

### Chicken & Marinade
+ ___2___ Egg Whites
+ ___3tbsp___ Dark Soy Sauce
+ ___3tbsp___ Chinese Cooking Wine
+ ___3tbsp___ Vodka
+ ___1/4tsp___ Baking Soda
+ ___3tbsp___ Cornstarch
+ ___1 1/2lb___ Chicken `breast or thigh, 1in chunks`

### Breading
+ ___1cup___ All-Purpose Flour
+ ___1cup___ Cornstarch
+ ___1tsp___ Baking Powder
+ ___1tsp___ Salt

### Sauce
+ ___6___ Scallions `thin sliced, separated`
+ ___4___ Garlic Cloves `minced`
+ ___2in___ Ginger Nugget `minced`
+ ___12___ Arbol Chiles `adjust to taste`
+ ___4tbsp___ Dark Soy Sauce
+ ___4tbsp___ Chinese Cooking Wine
+ ___4tbsp___ Rice Wine Vinegar
+ ___4tbsp___ Chicken Stock
+ ___1/4cup___ Sugar
+ ___1tsp___ Roasted Sesame Oil
+ ___1tbsp___ Cornstarch

## Preparation
1. In a medium bowl, beat `egg whites` until frothy. Mix in remaining `marinade` ingredients, reserving `chicken, baking soda, and cornstarch.`
2. Divide `marinade` equally between two bowls. Add remaining `marinade` ingredients to one, toss to coat, and chill for at least ___30min___.
3. In a shallow, wide pan, combine `breading` ingredients and whisk until mixed thorougly.
4. Slowly add reserved marinade to breading, massaging by hand to form small clumps. Set aside.
5. In a small bowl, combine `sauce` ingredients, reserving `vegetables.` Mix well.
6. Oil and heat a pan over ___medium-high__ heat, saute reserved `vegetables` until fragrant, then add in sauce mixture and allow to thicken.
7. Heat ___1 quart___ of vegetable oil in a deep pot to ___375°F___. Toss `chicken` in breading and fry in batches, around ___4min___ per batch. Drain and allow to dry, storing in a low oven to maintain warmth.
8. When all `chicken` is cooked through, combine and toss in sauce. Serve over rice.